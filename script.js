"use strict";

const themeButton = document.getElementById('themeButton');
const body = document.body;

const savedTheme = localStorage.getItem('theme');
if (savedTheme) {
     body.classList.add(savedTheme);
    }

themeButton.addEventListener('click', () => {
    if (body.classList.contains('dark')) {
        body.classList.remove('dark');
        body.classList.add('light');
        localStorage.setItem('theme', 'light');
        } else {
            body.classList.remove('light');
            body.classList.add('dark');
            localStorage.setItem('theme', 'dark');
            }
});